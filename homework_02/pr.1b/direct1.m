function inputf = direct1(t, y, p)

  inputf = [-p(1)*y(1)*y(2); p(1)*y(1)*y(2) - p(2)*y(2)];
  
end